import snow_cli.resorts.base as base

class Kirkwood(base.VailResort):
    html_url = "http://www.kirkwood.com/mountain/snow-and-weather-report.aspx"
    rss_url = "http://www.kirkwood.com/rss/snowreport.aspx"



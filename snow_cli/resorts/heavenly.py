import snow_cli.resorts.base as base

class Heavenly(base.VailResort):
    html_url = "http://www.skiheavenly.com/the-mountain/snow-report/snow-report.aspx"
    rss_url = "http://www.skiheavenly.com/rss/snowreport.aspx"



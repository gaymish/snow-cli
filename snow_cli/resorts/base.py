import requests
from bs4 import BeautifulSoup
import sys
import re

import snow_cli.exceptions

class VailResort(object):

    def __init__(self, rss_mode=False):
        self.rss_mode = rss_mode

    def _get_url(self):
        if self.rss_mode:
            return self.rss_url
        return self.html_url

    def read_from_file(self):
        with open(self._cache_file, 'r') as f1:
            self._raw_data = f1.read()
        self._read_file = True

    def set_cache_file(self, cf):
        self._cache_file = cf

    def _cleanup_html(self, s):
        s = s.replace("\n                                                ","")
        s = re.sub("[ ]{2,}", " ", s)
        return s.strip()

    def _cleanup_rss(self, s):
        s = re.sub(" - ", "<br/>", s)
        s = re.sub("&lt;", "<", s)
        s = re.sub("&gt;", ">", s)
        return s

    def fetch(self):
        if hasattr(self, "_read_file"):
            return
        headers = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.90 Safari/537.36",
        }
        r = requests.get(self._get_url(), headers=headers)
        r.raise_for_status()

        self._raw_data = r.text.encode('utf-8')
        if hasattr(self, "_cache_file"):
            with open(self._cache_file, 'w') as f:
                f.write(r.text)

    def _rss_weather_report(self):
        res = {}
        soup = BeautifulSoup(self._raw_data)
        data = self._cleanup_rss(soup.rss.channel.item.description.get_text())

        for i in data.split("<br/>"):
            tmp = i.split(":")
            res[tmp[0].strip()] = ":".join(tmp[1:]).strip()

        return res

    def weather_report(self):
        if not self._raw_data:
            raise snow_cli.exceptions.SnowCliCannotFetchException("No data available to process")

        if self.rss_mode:
            for k, v in self._rss_weather_report().items():
                print("{}: {}".format(k, v))
        else:
            print(self._html_weather_report())

    def _html_weather_report(self):
        res = {}
        soup = BeautifulSoup(self._raw_data)
        report = soup.find("div", {"id": "snowReport"})
        res.update(self._new_snow(report.find("div", {"class": "newSnow"})))
        res.update(self._terrain_report(report.find("div", {"class": "terrain"})))
        res.update(self._snow_conditions(report.find("div", {"class": "snowConditions"})))
        return res

    def _new_snow(self, soup):
        keys = ["last_24h", "overnight", "last_48h", "last_7d"]
        values = []
        for i in soup.table.tbody.find_all("tr"):
            values.append(self._cleanup_html(i.find("span").get_text()))

        return dict(zip(keys, values))

    def _terrain_report(self, soup):
        keys = ["terrain_open", "acres_open", "lifts_open", "runs_open"]
        values = []
        for i in soup.find("tbody").find_all("tr"):
            values.append(self._cleanup_html(i.find_all("td")[1].get_text()))

        return dict(zip(keys, values))

    def _snow_conditions(self, soup):
        return {"snow_type": self._cleanup(soup.find("tbody").find_all("td")[0].get_text())}


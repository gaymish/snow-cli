import snow_cli.resorts.kirkwood as Kirkwood
import snow_cli.resorts.heavenly as Heavenly
import snow_cli.resorts.northstar as NorthStar
import sys

reports = {
    "kirkwood": Kirkwood.Kirkwood,
    "heavenly": Heavenly.Heavenly,
    "northstar": NorthStar.NorthStar,
}

def weather_reporter(r, f=None):
    report_instance = reports.get(r, None)
    if not report_instance:
        print("Can't look up resort: {}".format(r))
        sys.exit(2)
    resort = report_instance(True)
    resort.set_cache_file("{}.local".format(r))
    if f:
        resort.read_from_file()
    resort.fetch()
    resort.weather_report()

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Expect at least the resort to be passed")
        sys.exit(1)
    cache = None
    if "--cache" in sys.argv:
        cache=True
    weather_reporter(sys.argv[1], cache)
else:
    raise ImportError("This file isn't to be imported")
